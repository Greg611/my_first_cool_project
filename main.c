#include <stdio.h>
#include <stdlib.h>

int uppercase_main(int argc,char* argv[]);
char* uppercase_sub(char* str);

int main(int argc, char* argv[]){
    uppercase_main(argc,argv);
    return 0;
    }


int uppercase_main(int argc,char* argv[]){
    if(argc==1){
        printf("Please give an argument\n");
        return;
    }
    for(int i=1;i<argc;i++){
        printf(uppercase_sub(argv[i]));
        printf("\n");
    }
    return;
}

char* uppercase_sub(char* str){
    for(int i=0;str[i]!='\0';i++){
        if((str[i]>='a')&&(str[i]<='z')){
            str[i]=str[i]+('A'-'a');
        }
    }
    return str;
}
